class UserGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :createAt, :visibility, :members
end
