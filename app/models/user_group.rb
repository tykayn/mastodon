# == Schema Information
#
# Table name: user_groups
#
#  id             :bigint(8)        not null, primary key
#  name           :string           not null
#  createdAt      :datetime
#  visibility     :string
#  account_id     :integer
#  creator_id     :integer
#  statuses_count :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class UserGroup < ApplicationRecord
end
