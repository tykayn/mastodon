import initialState from '../initial_state';
import React, { Fragment } from 'react';
import configureStore from '../store/configureStore';

import { Provider } from 'react-redux';

import PropTypes from 'prop-types';
import { hydrateStore } from '../actions/store';
import { addLocaleData, IntlProvider } from 'react-intl';
import { getLocale } from '../locales';

const { localeData, messages } = getLocale();
addLocaleData(localeData);

const store = configureStore();

if (initialState) {
  store.dispatch(hydrateStore(initialState));
}

export default class GalleryContainer extends React.PureComponent {
  static propTypes = {
    locale: PropTypes.string.isRequired,
  };

  render() {
    const { locale } = this.props;
    return (

      <IntlProvider
        locale={locale}
        messages={messages}
      >
        <Provider store={store}>
          <Fragment >
            <Gallery ></Gallery >
          </Fragment >
        </Provider >

      </IntlProvider >
    );
  }
}
