import React from 'react';

import PropTypes from 'prop-types';

const following = ['bidule', 'chose', 'truc'];

export default class Conversation extends React.Component {

  static propTypes = {
    following: PropTypes.array,
    // conversations: PropTypes.array,
  };

  static defaultProps = {
    following: following,
  };

  openConversationWith(name) {
    console.log('openConversationWith name', name);
  }

  render() {

    return this.props.following.map(elem =>

      (<li className='user-item'>
        <div
          className='username'
          onClick={this.openConversationWith(elem)}
        >
          Machin {elem}
        </div >
        <div className='last-active'>3 min</div >
      </li >),
    );
  };

}
