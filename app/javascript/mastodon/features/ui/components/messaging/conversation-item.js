import React from 'react';

export default class ConversationItem extends React.PureComponent {

  following = [];

  render() {

    const list = (
      <li className='conversations_item has-new-message'>
        <div className='title'>
          <i
            role='img'
            className='fa fa-envelope column-header__icon fa-fw'
          />
          Un Gens
          <span className='new-message-counter'>
          (3)</span >
          <button className='btn-small'>
            <i
              role='img'
              className='fa fa-caret-down column-header__icon fa-fw'
            />
          </button >
        </div >
        <div className='conversation_stream'>
          <div className='message theirs'>
            <p >oh hello there! 😋 </p >
            <div className='arrow-down' />
          </div >
          <div className='message mine'>
            <p >General Emoji</p >
            <div className='arrow-down' />
          </div >
          <div className='message theirs'>
            <p >we just achieved comedy</p >
            <div className='arrow-down' />
          </div >
        </div >
        <div className='conversation_input'>
          <form
            action='#'
            onSubmit={this.submitCompose()}
          >
            <textarea
              name='messager'
              id=''
              cols='15'
              rows='3'
              className='messager-textarea'
              placeholder='allez dis nous tout'

            />
            <input
              type='submit'
              name='submit'
              value='Send'
            />
          </form >
        </div >
      </li >
    );
    return list;

  }

}
