import React from 'react';
import PropTypes from 'prop-types';

export default class ContactsList extends React.PureComponent {

  static propTypes = {
    showList        : PropTypes.bool,
    contactList     : PropTypes.array,
    conversationList: PropTypes.array,
  };
  static defaultProps = {
    showList   : true,
    contactList: ['machin', 'bidule', 'chuck norris'],
  };

  constructor(props) {
    super(props);
    this.state = {
      showList        : true,
      contactList     : ['machin', 'bidule', 'chuck norris'],
      conversationList: ['machin', 'bidule', 'chuck norris'],
    };
  }

  submitCompose() {
    console.log('submit message');
  }

  toggleList = () => {
    console.log('toggle');
    this.setState((state) => {
      console.log('state.showList', state.showList);
      return {
        showList: !state.showList,
      };
    });
  };

  render() {

    // return (
    //   <div >
    //     liste de contacts
    //   </div >
    // );

    const renderedList = this.state.contactList.forEach(elem => {
      return (
        <li className='contact-item'>
          {elem}
        </li >
      );
    });
    return (
      <div className='messaging-container'>
        <div className='messaging-box'>
          <div className='title column-header'>
            <i
              role='img'
              className='fa fa-envelope column-header__icon fa-fw'
            />
            Messaging box

          </div >
          <div className='user-list column-header'>
            <h2 className='title'>la liste de {this.state.contactList.lengh} contacts

              <button
                className='btn btn-primary'
                onClick={this.toggleList}
              >
                <i className='fa fa-caret-up' />
              </button >
            </h2 >
            {this.state.showList && (
              <div className='contact-list-container'>
                <h3 >show list</h3 >
                <ul className='contact-list'>
                  {renderedList}
                </ul >
              </div >
            )}
          </div >

        </div >
      </div >
    );
  }

}
