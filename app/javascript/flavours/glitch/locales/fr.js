import inherited from 'mastodon/locales/fr.json';

const messages = {
  'navigation_bar.app_settings': 'Config de ouf',
  'tabs_bar.local_timeline': 'Bilveusées locales',
  'tabs_bar.federated_timeline': 'La mine de sel',
  'compose_form.publish': getRandomTermPouet(),
  'compose_form.save_changes': getRandomTermPouet(),
  getRandomTermPouet:getRandomTermPouet,
};

export function getRandomTermPouet() {
  let items = ['Pouet', 'Révolte', 'Couillère', 'Allez viens', 'On est bien', 'Sapristi', 'PWOUINNNN', 'Squalala', 'C\'est parti', 'Allez', 'Roulez jeunesse', 'Magnéto', 'Illuminer le monde'];
  return items[Math.floor(Math.random() * items.length)];
}

export default Object.assign({}, inherited, messages);
