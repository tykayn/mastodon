#!/bin/bash
## script must be run with the mastodon user
echo "dumping database before update":
cd && pg_dump -Fc mastodon -f mastodon-prod-db-$(date +%y-%m-%d).dump
echo "dumping database is done ":
ls -lArtH
cd /home/mastodon/live/
echo "ruby version is":
ruby -v
echo "needed version is":
cat .ruby-version
echo "########## time to update mastodon. you need to pull your git branch before running this script."
git remote -v
echo "########## install back end updates "
RAILS_ENV=production bundle install
RAILS_ENV=production bundle exec rake db:migrate
echo "########## install front end updates "
RAILS_ENV=production bundle exec rails assets:precompile
echo "########## check the services "
echo "########## now time to restart web services "
