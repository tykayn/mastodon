#!/bin/bash
## script must be run with the mastodon user

echo "ruby version is":
ruby -v
echo "needed version is":
cat .ruby-version
echo "########## time to update mastodon. you need to pull your git branch before running this script."
git remote -v
git pull origin master
echo "########## install back end updates "
bundle install
bundle exec rake db:migrate
echo "########## install front end updates "
bundle exec rails assets:precompile
echo "########## check the services "
echo "########## now time to restart web services "
foreman start
