#!/bin/bash

echo "########## time to check the services, this script must be run with an user able to manage services, someone with sudo rights for example"
sudo service postgresql status
sudo service mastodon-* status
echo "########## run:
sudo service mastodon-* restart

and check that everything is working fine

sudo journalctl -feu mastodon-*

"
