require 'rails_helper'

RSpec.describe "user_groups/index", type: :view do
  before(:each) do
    assign(:user_groups, [
      UserGroup.create!(
        :name => "Name",
        :visibility => "Visibility",
        :members => ""
      ),
      UserGroup.create!(
        :name => "Name",
        :visibility => "Visibility",
        :members => ""
      )
    ])
  end

  it "renders a list of user_groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Visibility".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
