class CreateUserGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :user_groups do |t|
      t.string :name, null: false
      t.datetime :createdAt
      t.string :visibility
      t.integer :account_id
      t.integer :creator_id
      t.integer :statuses_count

      t.timestamps
    end
  end
end
