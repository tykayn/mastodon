# frozen_string_literal: true
module Mastodon
  module Version
    module_function
    def source_base_url
      'https://framagit.org/tykayn/mastodon'
    end
  end
end
